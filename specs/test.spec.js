const {sendRequest} = require('../helpers/api.helper');
const testData = require('../config/data.json');

describe('API Test Suite', () => {

    it('GET post/1', async () =>{
        const response = await sendRequest('posts/1');
        
        expect(response.data.userId).to.equal(1);
        expect(response.data.id).to.equal(1);
        expect(response.status).to.equal(200);

        console.log(response.data);
        console.log(response.status);
    });

    it('POST posts', async () =>{
        const response = await sendRequest('posts', testData, 'post', {'Content-type': 'application/json; charset=UTF-8'});
                
        expect(response.data.userId).to.equal(1);
        expect(response.data.title).to.equal('foo');
        expect(response.data.body).to.equal('bar');
        expect(response.status).to.equal(201);

        console.log(response.data);
        console.log(response.status);
        //console.log(response.headers);
    });
});